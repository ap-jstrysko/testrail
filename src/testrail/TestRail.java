/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testrail;
import com.gurock.testrail.APIClient;
import com.gurock.testrail.APIException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
/**
 *
 * @author Jesse Strysko
 */

/**
 *
 * @author Jesse Strysko
 */
public class TestRail {

	public static void addTRresult(String verdict, String caseid, String notes,String version) throws MalformedURLException{
        TestRail.addTRresult(verdict, caseid, notes, version, "4");
    }
    
    public static void addTRresult(String verdict, String caseid, String notes) throws MalformedURLException{
    	TestRail.addTRresult(verdict, caseid, notes, "None");
    }
    
    public static void addTRresult(String verdict, String caseid, String notes,String version, String runID) throws MalformedURLException{
        try {
            // TODO code application logic here
            // System.out.println("TestRail 1.1");
            
            int result =4;
            if (verdict.contains("Pass")){result = 1;}
            else if (verdict.contains("Blocked")){result=2;}
            else if (verdict.contains("Fail")){result=5;}
            
            APIClient client = new APIClient("https://appointmentplus.testrail.com/");
            client.setUser("qa@appointmentplus.com");
            client.setPassword("Login2it");
            Map data = new HashMap();
            data.put("status_id", result);
            data.put("comment", notes);
            data.put("version", version);
            JSONObject r = (JSONObject) client.sendPost("add_result_for_case/"+runID+"/"+caseid, data);

            System.out.println(r);

        } catch (IOException ex) {
            Logger.getLogger(TestRail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (APIException ex) {
            Logger.getLogger(TestRail.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
  
}